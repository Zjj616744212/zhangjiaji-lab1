#include<iostream>
#include <omp.h>
#include <cmath>
#include <algorithm>
using namespace std;


int* quicks(int a[],int xx, int yy)
{
    int pivot  = a[yy];
    int i = xx - 1;

    for(int j=xx;j<yy;j++)
    {
        if(a[j]<=pivot)
        {
            i++;
			int t=a[i];a[i]=a[j];a[j]=t;
        }
    }
    int t=a[i+1];a[i+1]=a[yy];a[yy]=t;
    return (a+i+1);
}

void introMid(int a[], int *xx, int *yy, int len)
{
    int size = yy - xx;
    if(size < 16)      
    {
		int ups = xx - a;
		int down = yy - a;
    for(int i=ups+1;i<=down;i++)
    {
        int k = a[i];
        int j;
        for(j=i-1;j>=0;j--)
        {
            if(a[j]>k)
                a[j+1] = a[j];
            else
                break;
        }
        a[j+1] = k;
    }
        return;
    }
    if(len == 0)     
    {
        make_heap(xx,yy+1);
        sort_heap(xx,yy+1);
        return;
    }
    
    int *pis;
    
	if ((*xx < *(xx+size/2) && *(xx+size/2) < *yy) || (*yy < *(xx+size/2) && *(xx+size/2) < *xx)) 
       pis= (xx+size/2); 
    else if ((*(xx+size/2) < *xx && *xx < *yy) || (*yy < *xx && *xx < *(xx+size/2))) 
       pis=xx; 
    else
       pis=yy;
    int t=*pis;*pis=*yy;*yy=t;
    int* q = quicks(a,xx-a,yy-a);
#pragma omp parallel sections
{
	#pragma omp section
    introMid(a,xx,q-1,len-1);
	#pragma omp section
    introMid(a,q+1,yy,len-1);
}
}

void sorts(int a[],int *xx, int *yy,int threads)
{
	omp_set_num_threads(threads);
    int len = 2 * log(yy-xx);   
	double s1 = omp_get_wtime();
    introMid(a,xx,yy,len);
	double s2 = omp_get_wtime();
	cout<<"Execute time: "<< s2 - s1<<endl;
}

int main(int argc,char **argv)
{
   int threads= atoi(argv[1]);
	 int length= atoi(argv[2]);
	 int a[length];
    for(int i=0;i<length;i++)
    {
        a[i]=rand()%100+1;
    }
	cout<<"------before------"<<endl;
	for(int i=0;i<length;i++)
        cout<<a[i]<<" ";
	cout<<endl;
	
    sorts(a,a,a+(length-1),threads);
	
    cout<<"------Sorted------"<<endl;
    for(int i=0;i<length;i++)
        cout<<a[i]<<" ";
	cout<<endl;
	
    return 0;
}
