#include<iostream>
#include<cstdlib>
#include <omp.h>
using namespace std;


int partition(int array[], int low, int high)
{
    while (low < high)
    {
       
        while (low < high&&array[low] <= array[high])
        {
            high--;
        }
        int temp = array[low];
        array[low] = array[high];
        array[high] = temp;
       
        while (low < high&&array[low] <= array[high])
        {
            low++;
        }
        temp = array[low];
        array[low] = array[high];
        array[high] = temp;
    }
    return low;
}

void quickSortHelp(int array[], int low, int high)
{
    if (low < high)
    {
        
        int location = partition(array, low, high);
#pragma omp parallel sections
{
	#pragma omp section
        quickSortHelp(array, low, location - 1);
		#pragma omp section
        quickSortHelp(array, location + 1, high);
}
	}
}

void quickSort(int array[], int n)
{
   
    quickSortHelp(array, 0, n - 1);
}

int main(int argc,char **argv)
{
     int threads= atoi(argv[1]);
	 int length= atoi(argv[2]);
	 int array[length];
    
    for(int i=0;i<length;i++)
    {
        array[i]=rand()%100+1;
    }
	cout<<"------before------"<<endl;
	for(int i=0;i<length;i++)
        cout<<array[i]<<" ";
	cout<<endl;
	
  omp_set_num_threads(threads);
  double s1 = omp_get_wtime();
    quickSort(array, length);
	double s2 = omp_get_wtime();
	cout<<"Execute time: "<< s2 - s1<<endl;
	
    cout<<"------Sorted------"<<endl;
   for(int i=0;i<length;i++)
        cout<<array[i]<<" ";
	cout<<endl;
    return 0;
}
